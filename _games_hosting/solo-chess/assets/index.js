(function polyfill() {
  const relList = document.createElement("link").relList;
  if (relList && relList.supports && relList.supports("modulepreload")) {
    return;
  }
  for (const link of document.querySelectorAll('link[rel="modulepreload"]')) {
    processPreload(link);
  }
  new MutationObserver((mutations) => {
    for (const mutation of mutations) {
      if (mutation.type !== "childList") {
        continue;
      }
      for (const node of mutation.addedNodes) {
        if (node.tagName === "LINK" && node.rel === "modulepreload")
          processPreload(node);
      }
    }
  }).observe(document, { childList: true, subtree: true });
  function getFetchOpts(link) {
    const fetchOpts = {};
    if (link.integrity)
      fetchOpts.integrity = link.integrity;
    if (link.referrerPolicy)
      fetchOpts.referrerPolicy = link.referrerPolicy;
    if (link.crossOrigin === "use-credentials")
      fetchOpts.credentials = "include";
    else if (link.crossOrigin === "anonymous")
      fetchOpts.credentials = "omit";
    else
      fetchOpts.credentials = "same-origin";
    return fetchOpts;
  }
  function processPreload(link) {
    if (link.ep)
      return;
    link.ep = true;
    const fetchOpts = getFetchOpts(link);
    fetch(link.href, fetchOpts);
  }
})();
class Board {
  constructor(height = 8, width = 8, initialState, pieceCount = 0) {
    this.height = height;
    this.width = width;
    this.initialState = initialState;
    this.state = JSON.parse(JSON.stringify(this.initialState));
    this.pieceCount = pieceCount;
    this.initialPieceCount = this.pieceCount;
  }
  getWidth() {
    return this.width;
  }
  getHeight() {
    return this.height;
  }
  getCurrentState() {
    return this.state;
  }
  getPieceCount() {
    return this.pieceCount;
  }
  getIcon(budget) {
    throw new Error("Each board has its icons!");
  }
  resetBoard() {
    this.state = JSON.parse(JSON.stringify(this.initialState));
    this.pieceCount = this.initialPieceCount;
  }
  // "Abstract method"
  // Each board tells about its own target position by overriding this method
  isTargetPosition(position) {
    throw new Error("To check whether a position is target, you have to know which board you're in!");
  }
  // "Abstract method"
  // Each board type determines which moves are valid by overriding this method
  isValidMove(move) {
    throw new Error("To check whether a move is valid, you have to know which board you're in!");
  }
  doMove(move) {
    let newBudget = parseInt(this.state[move[0]][move[1]]) - 1;
    this.state[move[2]][move[3]] = newBudget.toString();
    this.state[move[0]][move[1]] = "";
    this.pieceCount--;
  }
}
class RookBoard extends Board {
  getIcon(budget) {
    switch (budget) {
      case "0":
        return '<div class="rook-div"><svg class="red" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><title>0</title><path d="M32 192V48c0-8.8 7.2-16 16-16h64c8.8 0 16 7.2 16 16V88c0 4.4 3.6 8 8 8h32c4.4 0 8-3.6 8-8V48c0-8.8 7.2-16 16-16h64c8.8 0 16 7.2 16 16V88c0 4.4 3.6 8 8 8h32c4.4 0 8-3.6 8-8V48c0-8.8 7.2-16 16-16h64c8.8 0 16 7.2 16 16V192c0 10.1-4.7 19.6-12.8 25.6L352 256l16 144H80L96 256 44.8 217.6C36.7 211.6 32 202.1 32 192zm176 96h32c8.8 0 16-7.2 16-16V224c0-17.7-14.3-32-32-32s-32 14.3-32 32v48c0 8.8 7.2 16 16 16zM22.6 473.4L64 432H384l41.4 41.4c4.2 4.2 6.6 10 6.6 16c0 12.5-10.1 22.6-22.6 22.6H38.6C26.1 512 16 501.9 16 489.4c0-6 2.4-11.8 6.6-16z"/></svg></div>';
      case "1":
        return '<div class="rook-div"><svg class="blue" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><title>1</title><path d="M32 192V48c0-8.8 7.2-16 16-16h64c8.8 0 16 7.2 16 16V88c0 4.4 3.6 8 8 8h32c4.4 0 8-3.6 8-8V48c0-8.8 7.2-16 16-16h64c8.8 0 16 7.2 16 16V88c0 4.4 3.6 8 8 8h32c4.4 0 8-3.6 8-8V48c0-8.8 7.2-16 16-16h64c8.8 0 16 7.2 16 16V192c0 10.1-4.7 19.6-12.8 25.6L352 256l16 144H80L96 256 44.8 217.6C36.7 211.6 32 202.1 32 192zm176 96h32c8.8 0 16-7.2 16-16V224c0-17.7-14.3-32-32-32s-32 14.3-32 32v48c0 8.8 7.2 16 16 16zM22.6 473.4L64 432H384l41.4 41.4c4.2 4.2 6.6 10 6.6 16c0 12.5-10.1 22.6-22.6 22.6H38.6C26.1 512 16 501.9 16 489.4c0-6 2.4-11.8 6.6-16z"/></svg></div>';
      case "2":
        return '<div class="rook-div"><svg class="green" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><title>2</title><path d="M32 192V48c0-8.8 7.2-16 16-16h64c8.8 0 16 7.2 16 16V88c0 4.4 3.6 8 8 8h32c4.4 0 8-3.6 8-8V48c0-8.8 7.2-16 16-16h64c8.8 0 16 7.2 16 16V88c0 4.4 3.6 8 8 8h32c4.4 0 8-3.6 8-8V48c0-8.8 7.2-16 16-16h64c8.8 0 16 7.2 16 16V192c0 10.1-4.7 19.6-12.8 25.6L352 256l16 144H80L96 256 44.8 217.6C36.7 211.6 32 202.1 32 192zm176 96h32c8.8 0 16-7.2 16-16V224c0-17.7-14.3-32-32-32s-32 14.3-32 32v48c0 8.8 7.2 16 16 16zM22.6 473.4L64 432H384l41.4 41.4c4.2 4.2 6.6 10 6.6 16c0 12.5-10.1 22.6-22.6 22.6H38.6C26.1 512 16 501.9 16 489.4c0-6 2.4-11.8 6.6-16z"/></svg></div>';
      case "":
        return "";
    }
  }
  isTargetPosition(position) {
    return position[0] === 0 && position[1] === this.width - 1;
  }
  isValidMove(move) {
    if (!(move[0] === move[2] && move[1] !== move[3]) && !(move[0] !== move[2] && move[1] === move[3])) {
      return false;
    }
    if (move[0] === move[2] && move[1] !== move[3]) {
      let l = parseInt(move[1]), r = parseInt(move[3]);
      if (l > r) {
        let swap = parseInt(move[1]);
        l = r;
        r = swap;
      }
      for (let i = l + 1; i < r; i++) {
        if (this.state[parseInt(move[0])][i] !== "")
          return false;
      }
    } else {
      let l = parseInt(move[0]), r = parseInt(move[2]);
      if (l > r) {
        let swap = parseInt(move[0]);
        l = r;
        r = swap;
      }
      for (let i = l + 1; i < r; i++) {
        if (this.state[i][parseInt(move[1])] !== "")
          return false;
      }
    }
    return true;
  }
}
class KnightBoard extends Board {
  getIcon(budget) {
    switch (budget) {
      case "0":
        return '<div class="knight-div"><svg class="red" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><title>0</title><path d="M96 48L82.7 61.3C70.7 73.3 64 89.5 64 106.5V238.9c0 10.7 5.3 20.7 14.2 26.6l10.6 7c14.3 9.6 32.7 10.7 48.1 3l3.2-1.6c2.6-1.3 5-2.8 7.3-4.5l49.4-37c6.6-5 15.7-5 22.3 0c10.2 7.7 9.9 23.1-.7 30.3L90.4 350C73.9 361.3 64 380 64 400H384l28.9-159c2.1-11.3 3.1-22.8 3.1-34.3V192C416 86 330 0 224 0H83.8C72.9 0 64 8.9 64 19.8c0 7.5 4.2 14.3 10.9 17.7L96 48zm24 68a20 20 0 1 1 40 0 20 20 0 1 1 -40 0zM22.6 473.4c-4.2 4.2-6.6 10-6.6 16C16 501.9 26.1 512 38.6 512H409.4c12.5 0 22.6-10.1 22.6-22.6c0-6-2.4-11.8-6.6-16L384 432H64L22.6 473.4z"/></svg></div>';
      case "1":
        return '<div class="knight-div"><svg class="blue" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><title>1</title><path d="M96 48L82.7 61.3C70.7 73.3 64 89.5 64 106.5V238.9c0 10.7 5.3 20.7 14.2 26.6l10.6 7c14.3 9.6 32.7 10.7 48.1 3l3.2-1.6c2.6-1.3 5-2.8 7.3-4.5l49.4-37c6.6-5 15.7-5 22.3 0c10.2 7.7 9.9 23.1-.7 30.3L90.4 350C73.9 361.3 64 380 64 400H384l28.9-159c2.1-11.3 3.1-22.8 3.1-34.3V192C416 86 330 0 224 0H83.8C72.9 0 64 8.9 64 19.8c0 7.5 4.2 14.3 10.9 17.7L96 48zm24 68a20 20 0 1 1 40 0 20 20 0 1 1 -40 0zM22.6 473.4c-4.2 4.2-6.6 10-6.6 16C16 501.9 26.1 512 38.6 512H409.4c12.5 0 22.6-10.1 22.6-22.6c0-6-2.4-11.8-6.6-16L384 432H64L22.6 473.4z"/></svg></div>';
      case "2":
        return '<div class="knight-div"><svg class="green" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><title>2</title><path d="M96 48L82.7 61.3C70.7 73.3 64 89.5 64 106.5V238.9c0 10.7 5.3 20.7 14.2 26.6l10.6 7c14.3 9.6 32.7 10.7 48.1 3l3.2-1.6c2.6-1.3 5-2.8 7.3-4.5l49.4-37c6.6-5 15.7-5 22.3 0c10.2 7.7 9.9 23.1-.7 30.3L90.4 350C73.9 361.3 64 380 64 400H384l28.9-159c2.1-11.3 3.1-22.8 3.1-34.3V192C416 86 330 0 224 0H83.8C72.9 0 64 8.9 64 19.8c0 7.5 4.2 14.3 10.9 17.7L96 48zm24 68a20 20 0 1 1 40 0 20 20 0 1 1 -40 0zM22.6 473.4c-4.2 4.2-6.6 10-6.6 16C16 501.9 26.1 512 38.6 512H409.4c12.5 0 22.6-10.1 22.6-22.6c0-6-2.4-11.8-6.6-16L384 432H64L22.6 473.4z"/></svg></div>';
      case "":
        return "";
      default:
        return '<div class="knight-div"><svg class="black" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><title>' + budget + '</title><path d="M96 48L82.7 61.3C70.7 73.3 64 89.5 64 106.5V238.9c0 10.7 5.3 20.7 14.2 26.6l10.6 7c14.3 9.6 32.7 10.7 48.1 3l3.2-1.6c2.6-1.3 5-2.8 7.3-4.5l49.4-37c6.6-5 15.7-5 22.3 0c10.2 7.7 9.9 23.1-.7 30.3L90.4 350C73.9 361.3 64 380 64 400H384l28.9-159c2.1-11.3 3.1-22.8 3.1-34.3V192C416 86 330 0 224 0H83.8C72.9 0 64 8.9 64 19.8c0 7.5 4.2 14.3 10.9 17.7L96 48zm24 68a20 20 0 1 1 40 0 20 20 0 1 1 -40 0zM22.6 473.4c-4.2 4.2-6.6 10-6.6 16C16 501.9 26.1 512 38.6 512H409.4c12.5 0 22.6-10.1 22.6-22.6c0-6-2.4-11.8-6.6-16L384 432H64L22.6 473.4z"/></svg></div>';
    }
  }
  // The reduction with knights doesn't use a target position
  isTargetPosition(position) {
    return false;
  }
  isValidMove(move) {
    let capturingKnightY = move[0];
    let capturingKnightX = move[1];
    let capturedKnightY = move[2];
    let capturedKnightX = move[3];
    if (Math.abs(capturingKnightY - capturedKnightY) === 2 && Math.abs(capturingKnightX - capturedKnightX) === 1)
      return true;
    if (Math.abs(capturingKnightX - capturedKnightX) === 2 && Math.abs(capturingKnightY - capturedKnightY) === 1)
      return true;
    return false;
  }
}
const hardcoded = [
  [5, 6, 0],
  [5, 10, 0],
  [5, 21, 0],
  [5, 35, 0],
  [5, 60, 0],
  [5, 66, 0],
  [5, 77, 0],
  [6, 8, 0],
  [6, 12, 0],
  [6, 12, 0],
  [6, 19, 0],
  [6, 21, 11],
  [6, 23, 0],
  [6, 33, 0],
  [6, 35, 11],
  [6, 37, 0],
  [6, 49, 0],
  [6, 51, 0],
  [6, 53, 0],
  [6, 58, 0],
  [6, 62, 0],
  [6, 64, 0],
  [6, 68, 0],
  [6, 75, 0],
  [6, 77, 11],
  [6, 79, 0],
  [7, 5, 0],
  [7, 6, 11],
  [7, 14, 0],
  [7, 28, 0],
  [7, 56, 0],
  [7, 70, 0],
  [8, 8, 0],
  [8, 16, 0],
  [8, 20, 0],
  [8, 22, 0],
  [8, 26, 0],
  [8, 30, 0],
  [8, 34, 0],
  [8, 36, 0],
  [8, 48, 0],
  [8, 50, 0],
  [8, 52, 0],
  [8, 54, 0],
  [8, 61, 0],
  [8, 63, 11],
  [8, 65, 0],
  [8, 72, 0],
  [8, 76, 0],
  [8, 78, 0],
  [9, 6, 0],
  [9, 18, 0],
  [9, 24, 0],
  [9, 32, 0],
  [9, 63, 0],
  [9, 74, 0],
  [10, 9, 0],
  [10, 21, 0],
  [10, 37, 0],
  [10, 79, 0],
  [10, 79, 0],
  [12, 8, 0],
  [12, 20, 0],
  [12, 36, 0],
  [12, 78, 0],
  [14, 7, 0],
  [14, 21, 0],
  [14, 35, 0],
  [14, 77, 0],
  [16, 8, 0],
  [16, 22, 0],
  [16, 34, 0],
  [16, 76, 0],
  [18, 9, 0],
  [18, 23, 0],
  [18, 33, 0],
  [18, 75, 0],
  [19, 6, 0],
  [19, 20, 0],
  [19, 35, 0],
  [19, 62, 0],
  [19, 66, 0],
  [19, 78, 0],
  [20, 8, 0],
  [20, 22, 0],
  [20, 32, 0],
  [20, 37, 0],
  [20, 64, 0],
  [20, 68, 0],
  [20, 72, 0],
  [20, 76, 0],
  [21, 5, 0],
  [21, 6, 11],
  [21, 19, 0],
  [21, 20, 11],
  [21, 30, 11],
  [21, 35, 0],
  [21, 61, 0],
  [21, 62, 11],
  [21, 70, 0],
  [21, 74, 0],
  [21, 78, 11],
  [21, 79, 0],
  [22, 8, 0],
  [22, 22, 0],
  [22, 33, 0],
  [22, 64, 0],
  [22, 76, 0],
  [23, 6, 0],
  [23, 20, 0],
  [23, 62, 0],
  [23, 78, 0],
  [24, 9, 0],
  [24, 23, 0],
  [24, 34, 0],
  [24, 65, 0],
  [24, 75, 0],
  [26, 8, 0],
  [26, 22, 0],
  [26, 33, 0],
  [26, 64, 0],
  [26, 76, 0],
  [28, 7, 0],
  [28, 21, 0],
  [28, 34, 0],
  [28, 63, 0],
  [28, 77, 0],
  //
  [30, 6, 0],
  [32, 5, 0],
  [33, 7, 0],
  [34, 4, 0],
  [34, 9, 0],
  [35, 2, 11],
  [35, 7, 0],
  [36, 5, 0],
  [38, 6, 0],
  [40, 5, 0],
  [42, 6, 0],
  [30, 20, 0],
  [32, 19, 0],
  [33, 21, 0],
  [34, 18, 0],
  [34, 23, 0],
  [35, 16, 11],
  [35, 21, 0],
  [36, 19, 0],
  [38, 20, 0],
  [40, 19, 0],
  [42, 20, 0],
  [30, 76, 0],
  [32, 75, 0],
  [33, 77, 0],
  [34, 74, 0],
  [34, 79, 0],
  [35, 72, 11],
  [35, 77, 0],
  [36, 75, 0],
  [38, 76, 0],
  [40, 75, 0],
  [42, 76, 0],
  [58, 33, 0],
  [60, 34, 0],
  [62, 33, 0],
  [63, 30, 11],
  [63, 35, 0],
  [64, 32, 0],
  [64, 37, 0],
  [65, 35, 0],
  [66, 33, 0],
  [68, 34, 0],
  [70, 35, 0],
  [72, 5, 0],
  [74, 6, 0],
  [76, 6, 0],
  [77, 2, 11],
  [77, 7, 0],
  [78, 4, 0],
  [78, 9, 0],
  [79, 7, 0],
  [80, 5, 0],
  [82, 6, 0],
  [84, 7, 0],
  [72, 19, 0],
  [74, 20, 0],
  [76, 20, 0],
  [77, 16, 11],
  [77, 21, 0],
  [78, 18, 0],
  [78, 23, 0],
  [79, 21, 0],
  [80, 19, 0],
  [82, 20, 0],
  [84, 21, 0],
  [72, 75, 0],
  [74, 76, 0],
  [76, 76, 0],
  [77, 72, 11],
  [77, 77, 0],
  [78, 74, 0],
  [78, 79, 0],
  [79, 77, 0],
  [80, 75, 0],
  [82, 76, 0],
  [84, 77, 0],
  [30, 35, 0],
  [32, 36, 0],
  [34, 35, 0],
  [36, 34, 0],
  [36, 36, 11],
  [38, 35, 0],
  [40, 36, 0],
  [41, 33, 2],
  [41, 38, 2],
  [42, 35, 0],
  [44, 7, 0],
  [46, 8, 0],
  [48, 7, 0],
  [50, 6, 0],
  [50, 8, 11],
  [52, 7, 0],
  [54, 8, 0],
  [55, 5, 2],
  [55, 10, 2],
  [56, 7, 0],
  [44, 21, 0],
  [46, 22, 0],
  [48, 21, 0],
  [50, 20, 0],
  [50, 22, 11],
  [52, 21, 0],
  [54, 22, 0],
  [55, 19, 2],
  [55, 24, 2],
  [56, 21, 0],
  [44, 77, 0],
  [46, 78, 0],
  [48, 77, 0],
  [50, 76, 0],
  [50, 78, 11],
  [52, 77, 0],
  [54, 78, 0],
  [55, 75, 2],
  [55, 80, 2],
  [56, 77, 0],
  [30, 64, 0],
  [32, 65, 0],
  [33, 62, 0],
  [34, 64, 0],
  [35, 61, 0],
  [35, 62, 11],
  [36, 64, 0],
  [37, 62, 0],
  [38, 65, 0],
  [40, 64, 0],
  [42, 63, 0],
  [44, 64, 0],
  [46, 65, 0],
  [47, 62, 0],
  [48, 64, 0],
  [49, 61, 0],
  [49, 62, 11],
  [50, 64, 0],
  [51, 62, 0],
  [52, 65, 0],
  [54, 64, 0],
  [56, 63, 0],
  [58, 64, 0],
  [60, 65, 0],
  [61, 62, 0],
  [62, 64, 0],
  [63, 61, 0],
  [63, 62, 11],
  [64, 64, 0],
  [65, 62, 0],
  [66, 65, 0],
  [68, 64, 0],
  [70, 63, 0],
  [86, 8, 0],
  [88, 9, 0],
  [89, 6, 0],
  [90, 8, 0],
  [91, 5, 0],
  [91, 6, 11],
  [92, 8, 0],
  [93, 6, 0],
  [94, 9, 0],
  [96, 8, 0],
  [98, 7, 0],
  [43, 38, 2],
  [44, 36, 0],
  [46, 35, 0],
  [48, 34, 0],
  [48, 36, 11],
  [50, 35, 0],
  [52, 36, 0],
  [54, 35, 0],
  [56, 34, 0],
  [57, 10, 2],
  [58, 8, 0],
  [60, 7, 0],
  [62, 6, 0],
  [62, 8, 11],
  [64, 7, 0],
  [66, 8, 0],
  [68, 7, 0],
  [70, 6, 0],
  [57, 24, 2],
  [58, 22, 0],
  [60, 21, 0],
  [62, 20, 0],
  [62, 22, 11],
  [64, 21, 0],
  [66, 22, 0],
  [68, 21, 0],
  [70, 20, 0],
  [57, 80, 2],
  [58, 78, 0],
  [60, 77, 0],
  [62, 76, 0],
  [62, 78, 11],
  [64, 77, 0],
  [66, 78, 0],
  [68, 77, 0],
  [70, 76, 0],
  [76, 44, 0],
  [75, 46, 0],
  [78, 47, 0],
  [76, 48, 0],
  [78, 49, 11],
  [79, 49, 0],
  [76, 50, 0],
  [78, 51, 0],
  [75, 52, 0],
  [76, 54, 0],
  [77, 56, 0],
  [90, 30, 0],
  [89, 32, 0],
  [92, 33, 0],
  [90, 34, 0],
  [92, 35, 11],
  [93, 35, 0],
  [90, 36, 0],
  [92, 37, 0],
  [89, 38, 0],
  [90, 40, 0],
  [91, 42, 0],
  [90, 44, 0],
  [89, 46, 0],
  [92, 47, 0],
  [90, 48, 0],
  [92, 49, 11],
  [93, 49, 0],
  [90, 50, 0],
  [92, 51, 0],
  [89, 52, 0],
  [90, 54, 0],
  [91, 56, 0],
  [90, 58, 0],
  [89, 60, 0],
  [92, 61, 0],
  [90, 62, 0],
  [92, 63, 11],
  [93, 63, 0],
  [90, 64, 0],
  [92, 65, 0],
  [89, 66, 0],
  [90, 68, 0],
  [91, 70, 0],
  [118, 16, 0],
  [117, 18, 0],
  [120, 19, 0],
  [118, 20, 0],
  [120, 21, 11],
  [121, 21, 0],
  [118, 22, 0],
  [120, 23, 0],
  [117, 24, 0],
  [118, 26, 0],
  [119, 28, 0],
  [120, 30, 0],
  [118, 31, 0],
  [120, 32, 0],
  [118, 33, 11],
  [72, 34, 0],
  [74, 33, 0],
  [75, 38, 0],
  [76, 34, 0],
  [76, 36, 0],
  [76, 40, 0],
  [77, 42, 0],
  [78, 33, 0],
  [78, 35, 11],
  [78, 37, 0],
  [79, 35, 0],
  [86, 20, 0],
  [88, 19, 0],
  [89, 24, 0],
  [90, 20, 0],
  [90, 22, 0],
  [90, 26, 0],
  [91, 28, 0],
  [92, 19, 0],
  [92, 21, 11],
  [92, 23, 0],
  [93, 21, 0],
  [114, 6, 0],
  [116, 5, 0],
  [117, 10, 0],
  [118, 6, 0],
  [118, 8, 0],
  [118, 12, 0],
  [119, 14, 0],
  [120, 5, 0],
  [120, 7, 11],
  [120, 9, 0],
  [121, 7, 0],
  [72, 62, 0],
  [74, 61, 0],
  [75, 64, 0],
  [76, 62, 0],
  [77, 64, 11],
  [77, 65, 0],
  [78, 58, 0],
  [78, 62, 0],
  [79, 60, 0],
  [79, 64, 0],
  [100, 76, 0],
  [102, 75, 0],
  [103, 78, 0],
  [104, 76, 0],
  [105, 78, 11],
  [105, 79, 0],
  [106, 72, 0],
  [106, 76, 0],
  [107, 74, 0],
  [107, 78, 0],
  [86, 76, 0],
  [88, 75, 0],
  [89, 78, 0],
  [90, 72, 0],
  [90, 76, 0],
  [91, 74, 0],
  [91, 78, 11],
  [91, 79, 0],
  [92, 76, 0],
  [93, 78, 0],
  [94, 75, 0],
  [96, 76, 0],
  [98, 77, 0],
  [100, 8, 0],
  [102, 9, 0],
  [103, 6, 0],
  [104, 8, 0],
  [105, 5, 0],
  [105, 6, 11],
  [105, 10, 0],
  [105, 14, 0],
  [106, 8, 0],
  [106, 12, 0],
  [107, 6, 0],
  [108, 9, 0],
  [110, 8, 0],
  [112, 7, 0],
  [106, 16, 0],
  [107, 18, 0],
  [105, 19, 0],
  [103, 20, 0],
  [108, 20, 0],
  [105, 21, 0],
  [110, 21, 11],
  [107, 22, 0],
  [106, 24, 0],
  [107, 26, 0],
  [106, 28, 0],
  [105, 30, 0],
  [104, 32, 0],
  [105, 34, 0],
  [104, 36, 11],
  [106, 36, 0],
  [106, 36, 0],
  [105, 38, 0],
  [104, 40, 0],
  [102, 41, 2],
  [107, 41, 2],
  [105, 42, 0],
  [102, 43, 2],
  [104, 44, 0],
  [105, 46, 0],
  [104, 48, 11],
  [106, 48, 0],
  [105, 50, 0],
  [104, 52, 0],
  [105, 54, 0],
  [106, 56, 0],
  [107, 58, 0],
  [106, 60, 0],
  [107, 62, 0],
  [105, 63, 0],
  [110, 63, 11],
  [103, 64, 0],
  [108, 64, 0],
  [105, 65, 0],
  [107, 66, 0],
  [106, 68, 0],
  [105, 70, 0]
];
function getHardcoded() {
  return hardcoded;
}
function rooksReduction(numberOfNodes2, edges2, k) {
  const graphOrder = numberOfNodes2;
  const graphEdges = edges2;
  const graphSize = graphEdges.length;
  const delta = 2 * k - graphOrder;
  let yOffset = 0;
  let height = graphSize + 1;
  let width = graphOrder + graphSize + Math.abs(delta) + 1;
  let pieceCount = 0;
  let initialState = new Array(height).fill("").map(() => new Array(width).fill(""));
  for (let i = 0; i < graphOrder; i++) {
    initialState[0][i] = "2";
    pieceCount++;
  }
  for (let i = 1; i <= graphSize; i++) {
    initialState[i][graphOrder + i - 1] = "2";
    pieceCount++;
  }
  for (let i = graphOrder + graphSize; i < width - 1; i++) {
    initialState[0][i] = "2";
    pieceCount++;
  }
  if (delta <= 0) {
    for (let i = 0; i < 2 * Math.abs(delta); i += 2) {
      addRow(initialState, i / 2 + 1);
      addColumn(initialState, graphOrder + graphSize + i + 1);
      initialState[i / 2 + 1][graphOrder + graphSize + i] = "2";
      initialState[i / 2 + 1][graphOrder + graphSize + i + 1] = "2";
      height++;
      width++;
      pieceCount += 2;
    }
    yOffset = Math.abs(delta);
  }
  initialState[0][width - 1] = "2";
  pieceCount++;
  for (let i = yOffset + 1; i < yOffset + graphSize + 1; i++) {
    let edge = graphEdges[i - yOffset - 1];
    let source = edge.split(" ")[0];
    let target = edge.split(" ")[1];
    initialState[i][source] = "2";
    initialState[i][target] = "2";
    pieceCount += 2;
  }
  for (let i = yOffset + 1; i < yOffset + 1 + 2 * graphSize; i += 2) {
    let numberOfRooks = 0;
    let found = false;
    for (let j = 0; j < width && !found; j++) {
      if (initialState[i][j] === "2")
        numberOfRooks++;
      if (numberOfRooks === 2) {
        addColumn(initialState, j);
        addColumn(initialState, j);
        addRow(initialState, i + 1);
        initialState[i][j] = "2";
        initialState[i + 1][j] = "2";
        initialState[i + 1][j + 1] = "2";
        width += 2;
        height++;
        pieceCount += 3;
        found = true;
      }
    }
  }
  return new RookBoard(height, width, initialState, pieceCount);
}
function knightsReduction() {
  let height = 127;
  let width = 85;
  let initialState = new Array(height).fill("").map(() => new Array(width).fill(""));
  let pieceCount = 0;
  getHardcoded().forEach((piece) => {
    initialState[piece[0]][piece[1]] = "" + piece[2];
    pieceCount++;
  });
  return new KnightBoard(height, width, initialState, pieceCount);
}
function addRow(initialState, index) {
  const boardWidth = initialState[0].length;
  const emptyRow = new Array(boardWidth).fill("");
  initialState.splice(index, 0, emptyRow);
}
function addColumn(initialState, index) {
  initialState.map((row) => {
    row.splice(index, 0, "");
  });
}
const status$1 = document.querySelector("#status");
let capturingPiece;
let capturingPieceCell;
let isMoving = false;

const emptyImg = document.createElement("img");
emptyImg.src = "";

function dragStart(e) {
  // If the player is already moving a piece
  if (isMoving) {
    if (capturingPieceCell.classList.contains("border"))
      capturingPieceCell.style.background = "lightblue";
    else
      capturingPieceCell.style.background = "white";
  }

  capturingPiece = e.target;

  while (!capturingPiece.classList.contains("rook-div") && !capturingPiece.classList.contains("knight-div")) {
    capturingPiece = capturingPiece.parentNode;
  }

  // I probably know why some non-draggable pieces can be moved
  // For now, prevent this annoying behaviour manually
  if (!capturingPiece.getAttribute('draggable')) {
    // This piece cannot be moved!
    e.preventDefault();
  }
  
  // Set a suitable drag image
  // Shape and color depend on the type of the piece you're moving
  const pieceImg = capturingPiece.firstChild;
  e.dataTransfer.setDragImage(pieceImg, 0, 0);
}
function dragOver(e) {
  e.preventDefault();
}
function dragDrop(e, board2) {
  e.stopPropagation();
  const wasEmpty = e.target.classList.contains("rooks-cell") || e.target.classList.contains("knights-cell");
  if (!wasEmpty) {
    let capturedPieceDiv = e.target;
    while (!capturedPieceDiv.classList.contains("rook-div") && !capturedPieceDiv.classList.contains("knight-div")) {
      capturedPieceDiv = capturedPieceDiv.parentNode;
    }
    let capturingPieceY = capturingPiece.parentNode.getAttribute("id").split("-")[1];
    let capturingPieceX = capturingPiece.parentNode.getAttribute("id").split("-")[2];
    let capturedPieceY = capturedPieceDiv.parentNode.getAttribute("id").split("-")[1];
    let capturedPieceX = capturedPieceDiv.parentNode.getAttribute("id").split("-")[2];
    let move = [capturingPieceY, capturingPieceX, capturedPieceY, capturedPieceX];
    if (board2.isValidMove(move)) {
      board2.doMove(move);
      updateView(move);
      status$1.innerHTML = "";
      if (board2.getPieceCount() === 1) {
        status$1.innerHTML = "You won!";
        status$1.style = "color: green; font-weight: bold;";
      }
    } else {
      status$1.innerHTML = "This is not a valid move!";
      status$1.style = "color: red; font-weight: bold;";
    }
  } else {
    status$1.innerHTML = "You must capture a piece!";
    status$1.style = "color: red; font-weight: bold;";
  }
  
  isMoving = false;
}

function click(e, board2) {
  if (!isMoving) {
    capturingPiece = e.target;

    while (!capturingPiece.classList.contains("rook-div") && !capturingPiece.classList.contains("knight-div")) {
      capturingPiece = capturingPiece.parentNode;
    }

    if (capturingPiece.getAttribute("draggable") === "true") {
      // Highlight the cell of the moving piece
      capturingPieceCell = capturingPiece.parentNode;
      capturingPieceCell.style.background = "yellow";

      isMoving = true;
    }
  }
  else {
    const wasEmpty = e.target.classList.contains("rooks-cell") || e.target.classList.contains("knights-cell");
    if (!wasEmpty) {
      let capturedPieceDiv = e.target;

      while (!capturedPieceDiv.classList.contains("rook-div") && !capturedPieceDiv.classList.contains("knight-div")) {
        capturedPieceDiv = capturedPieceDiv.parentNode;
      }
      let capturingPieceY = capturingPiece.parentNode.getAttribute("id").split("-")[1];
      let capturingPieceX = capturingPiece.parentNode.getAttribute("id").split("-")[2];
      let capturedPieceY = capturedPieceDiv.parentNode.getAttribute("id").split("-")[1];
      let capturedPieceX = capturedPieceDiv.parentNode.getAttribute("id").split("-")[2];
      let move = [capturingPieceY, capturingPieceX, capturedPieceY, capturedPieceX];
      if (board2.isValidMove(move)) {
        board2.doMove(move);
        updateView(move);
        status$1.innerHTML = "";
        if (board2.getPieceCount() === 1) {
          status$1.innerHTML = "You won!";
          status$1.style = "color: green; font-weight: bold;";
        }
      }
      else {
        status$1.innerHTML = "This is not a valid move!";
        status$1.style = "color: red; font-weight: bold;";
      }
    } else {
      status$1.innerHTML = "You must capture a piece!";
      status$1.style = "color: red; font-weight: bold;";
    }

    // Reset capturing piece's cell background color
    // Since there's only two types of cell (standard and border), colors can be reset manually
    if (capturingPieceCell.classList.contains("border"))
      capturingPieceCell.style.background = "lightblue";
    else
      capturingPieceCell.style.background = "#eee";
    isMoving = false;
  }
}
const boardDiv = document.querySelector("#chessboard");
let chessboard$1;
function viewBoard(board2, reduction2, reset) {
  var _a, _b;
  clearDiv(boardDiv);
  chessboard$1 = board2;
  if (reset) {
    chessboard$1.resetBoard();
  }
  let state = chessboard$1.getCurrentState();
  let table = document.createElement("table");
  table.setAttribute("cellspacing", "0");
  for (let i = 0; i < board2.getHeight(); i++) {
    let tr = document.createElement("tr");
    for (let j = 0; j < board2.getWidth(); j++) {
      let cell = document.createElement("td");
      cell.setAttribute("id", "cell-" + i + "-" + j);
      cell.setAttribute("class", "cell");
      cell.setAttribute("class", reduction2 + "-cell");
      if (reduction2 === "knights" && (i % 14 === 0 || j % 14 === 0)) {
        cell.classList.add("border");
      }

      // Handle drag events
      cell.addEventListener("dragstart", dragStart);
      cell.addEventListener("dragover", dragOver);
      cell.addEventListener("drop", (e) => dragDrop(e, chessboard$1));

      // Handle click events
      cell.addEventListener("click", (e) => click(e, chessboard$1));

      cell.innerHTML = chessboard$1.getIcon(state[i][j]);
      if (state[i][j] !== "") {
        if (parseInt(state[i][j]) > 0)
          (_a = cell.firstChild) == null ? void 0 : _a.setAttribute("draggable", true);
        cell.firstChild.firstChild.firstChild.innerHTML = state[i][j];
      }
      if (board2.isTargetPosition([i, j])) {
        (_b = cell.firstChild) == null ? void 0 : _b.setAttribute("draggable", false);
        cell.classList.add("target");
      }
      tr.appendChild(cell);
    }
    table.appendChild(tr);
  }
  boardDiv.appendChild(table);
}
function updateView(move) {
  let currentState = chessboard$1.getCurrentState();
  let capturingCell = document.getElementById("cell-" + move[0] + "-" + move[1]);
  let capturedCell = document.getElementById("cell-" + move[2] + "-" + move[3]);
  capturingCell.innerHTML = "";
  capturedCell.innerHTML = chessboard$1.getIcon(currentState[move[2]][move[3]]);
  capturedCell.firstChild.firstChild.firstChild.innerHTML = currentState[move[2]][move[3]];
  if (chessboard$1.isTargetPosition([parseInt(move[2]), parseInt(move[3])])) {
    capturedCell.firstChild.setAttribute("draggable", false);
    capturedCell.classList.add("target");
  } else if (currentState[move[2]][move[3]] !== "" && parseInt(currentState[move[2]][move[3]]) > 0) {
    capturedCell.firstChild.setAttribute("draggable", true);
  }
}
function clearDiv(divNode) {
  while (divNode.firstChild) {
    divNode.removeChild(divNode.firstChild);
  }
}
const reductionChoice = document.querySelector("#reduction-choice");
const addEdgesForm = document.querySelector("#add-edges-form");
const edgesTextarea = document.querySelector("#graph");
const buttons = document.querySelectorAll(".buttons");
const status = document.querySelector("#status");
const chessboard = document.querySelector("#chessboard");
const legenda = document.querySelector("#legenda");
const rooksInfo = document.querySelector("#rooks-info");
const knightsInfo = document.querySelector("#knights-info");
let reduction;
let numberOfNodes;
let edges = [];
let parameter;
let board;
document.querySelector("#rooks-button").addEventListener("click", () => {
  reduction = "rooks";
  reductionChoicePage();
});
document.querySelector("#knights-button").addEventListener("click", () => {
  reduction = "knights";
  reductionChoicePage();
});
document.querySelector("#generate").addEventListener("click", () => {
  generateBoard();
  chessboard.scrollIntoView();
});
document.querySelectorAll(".reload-button").forEach((button) => button.addEventListener("click", () => location.reload()));
document.querySelector("#reset-board").addEventListener("click", () => {
  status.innerHTML = "";
  resetBoard();
});
function reductionChoicePage() {
  reductionChoice.style.display = "none";
  if (reduction === "knights") {
    knightsInfo.style.display = "block";
    chessboard.style.display = "flex";
    legenda.style.display = "flex";
    status.style.display = "block";
    buttons.forEach((div) => div.style.display = "block");
    generateBoard();
  } else
    edgesPage();
}
function edgesPage() {
  addEdgesForm.style.display = "flex";
  buttons.forEach((div) => div.style.display = "block");
  edgesTextarea.value = "0 1\n0 2\n1 2\n2 3";
  document.querySelector("#parameter").value = 2;
  generateBoard();
  rooksInfo.style.display = "block";
  status.style.display = "block";
  chessboard.style.display = "flex";
  legenda.style.display = "flex";
  chessboard.scrollIntoView();
}
function generateBoard() {
  if (reduction === "rooks") {
    numberOfNodes = 0;
    edges = [];
    edgesTextarea.value.trim().split("\n").forEach((edge) => {
      edge = edge.trim();
      let u = edge.split(" ")[0];
      let v = edge.split(" ")[1];
      if (u > v) {
        let swap = u;
        u = v;
        v = swap;
        edge = u + " " + v;
      }
      if (!edges.includes(edge))
        edges.push(edge);
      numberOfNodes = Math.max(parseInt(u), parseInt(v));
    });
    numberOfNodes++;
    parameter = parseInt(document.querySelector("#parameter").value);
    if (parameter > numberOfNodes)
      parameter = numberOfNodes;
    board = rooksReduction(numberOfNodes, edges, parameter);
    viewBoard(board, reduction, true);
  } else if (reduction === "knights") {
    board = knightsReduction();
    viewBoard(board, reduction, false);
  }
}
function resetBoard() {
  viewBoard(board, reduction, true);
}
