'use strict';

var Palette = {
    empty: {img: "empty.png"},
    block: {img: "rock.png"},
    du: {img: "rail.du.png"},
    dl: {img: "rail.dl.png"},
    spawn_red: {img: "spawn.red.png"},
    spawn_blue: {img: "spawn.blue.png"},
    sciss: {img: "scissors.png"},
    arrival_red: {img: "arrival.red.png"},
    arrival_blue: {img: "arrival.blue.png"},
    arrival_done: {img: "arrival.done.png"},
    paint_red: {img: "paint.red.png"},
    paint_blue: {img: "paint.blue.png"},
    train_white: {img: "train.white.png"},
    train_red: {img: "train.red.png"},
    train_blue: {img: "train.blue.png"},
    train_purple: {img: "train.purple.png"},
}
function paletteCreate(paletteContainer, onReady){
    var p = $(paletteContainer);
    p.html("");
    for (var k in Palette)
        if (Palette.hasOwnProperty(k)){
            p.append(Palette[k].el = $("<img>").addClass("palette").attr("id", "palette_"+k).attr("src", "imgs/"+Palette[k].img)[0]);
        }
    return p;
}

class Playground{
    constructor(playgroundContainer, board, highlightRects = []){
        this.playgroundContainer = $(playgroundContainer);
        this.layers = { background: {}, main: {}, gadgetHighlight: {}, trace: {}, trail: {}, foreground: {}};
        this.options = {
            cellSize: 20,
            delayPlay: 100,
            delayFast: 10,
            trailFadeout: 0.1,
            trailSize: 20,
            trailAlpha: 0.3,
            highlightGadgetWidth: 3,
        };
        this.updateIntervalHandler = 0; // Interval handler
        this.board = board;
        this.highlightRects = highlightRects;

        this.animTargetDelay = -1;
        this.animLast = 0;

        this.initLayers();
        this.initBoardCallbacks();
        this.restart();
    }
    restart(){
        this.board.restart();
    }
    clear(){
        this.pause();
        this.playgroundContainer.html("").css("width", 0).css("height", 0);
    }
    trace() {
        this.traceClear();
        this.board.trainTrace();
    }
    traceClear(){
        this.layers.trace.ctx.clearRect(0, 0, this.layers.trace.ctx.canvas.width, this.layers.trace.ctx.canvas.height);
    }
    play(fast = false){
        this.pause();
        this.animTargetDelay = fast ? this.options.delayFast : this.options.delayPlay;
        this.animLast = 0;
        // this.updateIntervalHandler = setInterval(() => this.doStep(), this.animTargetDelay);
        this.animate();
    }
    pause(){
        // clearInterval(this.updateIntervalHandler);
        cancelAnimationFrame(this.updateIntervalHandler);
        this.animTargetDelay = -1;
    }
    animate(time){
        if (this.animTargetDelay == -1) return;
        if (time === undefined)
            this.animLast = 0;
        if (time-this.animLast >= this.animTargetDelay){
            this.doStep();
            this.animLast = time + (time-this.animLast) % this.animTargetDelay;
        }
        requestAnimationFrame(x => this.animate(x));
    }
    doStep(){
        this.board.doStep();
    }
    initLayers(){
        this.playgroundContainer.html("")
                .append(this.layers.background.el = $("<canvas>"))
                .append(this.layers.main.el = $("<canvas>"))
                .append(this.layers.gadgetHighlight.el = $("<canvas>"))
                .append(this.layers.trace.el = $("<canvas>"))
                .append(this.layers.trail.el = $("<canvas>"))
                .append(this.layers.foreground.el = $("<canvas>"))
        for (var layer in this.layers)
            if (this.layers.hasOwnProperty(layer)){
                this.layers[layer].el.addClass("canvas_"+layer);
                this.layers[layer].ctx = this.layers[layer].el[0].getContext("2d");
            }
    }
    initBoardCallbacks(){
        this.board.onSetup = (draw) => this.boardDrawBg(draw);
        this.board.cellDraw = (r,c) => this.boardCellDraw(r,c);
        this.board.trainDraw = (train,tracing) => this.boardtrainDraw(train, tracing);
        this.board.onBeforeTrainsDraw = () => this.boardOnBeforeTrainsDraw();
        this.board.onCrash = () => {
            alert("Crashed!");
            console.log("Crashed!");
            this.pause();
        }
        this.board.onFail = () => {
            alert("Failed!");
            console.log("Failed!");
            this.pause();
        }
        this.board.onSuccess = () => {
            alert("Success!");
            console.log("Success!");
            this.pause();
        }
    }

    boardDrawBg(draw){
        if (!draw) return;
        this.playgroundContainer.css("width", this.board.width * this.options.cellSize+"px").css("height", this.board.height * this.options.cellSize+"px");
        for (var layer in this.layers)
            if (this.layers.hasOwnProperty(layer)){
                this.layers[layer].ctx.canvas.width = this.board.width * this.options.cellSize;
                this.layers[layer].ctx.canvas.height = this.board.height * this.options.cellSize;
                this.layers[layer].ctx.clearRect(0, 0, this.layers[layer].ctx.canvas.width, this.layers[layer].ctx.canvas.height);
            }
        this.layers.trail.ctx.globalAlpha = this.options.trailAlpha;
        for (var r = 0; r < this.board.height; r++)
            for (var c = 0; c < this.board.width; c++) {
                var cell = this.board.board[r][c];
                var img = null;
                switch (cell.type) {
                    case BlockType.Rail:
                    case BlockType.Empty:
                        img = Palette.empty.el; break;
                    case BlockType.Rock:
                        img = Palette.block.el; break;
                    case BlockType.Spawner:
                        img = (cell.color == Colors.Red ? Palette.spawn_red.el : Palette.spawn_blue.el); break;
                    case BlockType.Splitter:
                        img = Palette.sciss.el; break;
                    case BlockType.Painter:
                        img = (cell.color == Colors.Red ? Palette.paint_red.el : Palette.paint_blue.el); break
                }
                if (img != null) {
                    if (cell.rotation != 0) {
                        this.layers.background.ctx.save();
                        this.layers.background.ctx.translate(c * this.options.cellSize + this.options.cellSize / 2, r * this.options.cellSize + this.options.cellSize / 2);
                        this.layers.background.ctx.rotate(cell.rotation * Math.PI / 180);
                        this.layers.background.ctx.drawImage(img, -this.options.cellSize / 2, -this.options.cellSize / 2, this.options.cellSize, this.options.cellSize);
                        this.layers.background.ctx.restore();
                    } else
                        this.layers.background.ctx.drawImage(img, c * this.options.cellSize, r * this.options.cellSize, this.options.cellSize, this.options.cellSize);
                }
            }
        for (var i = 0; i < this.highlightRects.length; i++) {
            this.layers.gadgetHighlight.ctx.beginPath();
            this.layers.gadgetHighlight.ctx.lineWidth = this.options.highlightGadgetWidth;
            this.layers.gadgetHighlight.ctx.strokeStyle = this.highlightRects[i].color;
            this.layers.gadgetHighlight.ctx.rect(this.highlightRects[i].c * this.options.cellSize + this.options.highlightGadgetWidth / 2, this.highlightRects[i].r * this.options.cellSize + this.options.highlightGadgetWidth / 2, this.highlightRects[i].w * this.options.cellSize - this.options.highlightGadgetWidth, this.highlightRects[i].h * this.options.cellSize - this.options.highlightGadgetWidth);
            this.layers.gadgetHighlight.ctx.stroke();
        }
    }
    boardCellDraw(r, c) {
        var cell = this.board.board[r][c];
        this.layers.main.ctx.clearRect(c * this.options.cellSize, r * this.options.cellSize, this.options.cellSize, this.options.cellSize);
        if (cell.type == BlockType.Rail) {
            for (var i = 0; i < cell.rail.length; i+=2) {
                var rot = 0, img = null;
                switch (cell.rail.substr(cell.rail.length-i-2, 2).toUpperCase()) {
                    case "LR": rot += 90;
                    case "DU": img = Palette.du.el; break;
                    case "DR": rot += 90;
                    case "RU": rot += 90;
                    case "LU": rot += 90;
                    case "DL": img = Palette.dl.el; break;
                }
                if (img != null) {
                    this.layers.main.ctx.save();
                    if (i+2 < cell.rail.length) this.layers.main.ctx.globalAlpha = 0.5;
                    this.layers.main.ctx.translate(c * this.options.cellSize + this.options.cellSize / 2, r * this.options.cellSize + this.options.cellSize / 2);
                    this.layers.main.ctx.rotate(rot * Math.PI / 180);
                    this.layers.main.ctx.drawImage(img, -this.options.cellSize / 2, -this.options.cellSize / 2, this.options.cellSize, this.options.cellSize);
                    this.layers.main.ctx.restore();
                }
            }
        } else {
            var img = null;
            switch (cell.type) {
                case BlockType.Arrival:
                    if (cell.color == Colors.Red)
                        img = Palette.arrival_red.el;
                    else if (cell.color == Colors.Blue)
                        img = Palette.arrival_blue.el;
                    else if (cell.color == Colors.Satisfied)
                        img = Palette.arrival_done.el;
                    else
                        throw "Unknown arrival color"
                    break;
            }
            if (img != null) {
                this.layers.main.ctx.save();
                this.layers.main.ctx.translate(c * this.options.cellSize + this.options.cellSize / 2, r * this.options.cellSize + this.options.cellSize / 2);
                this.layers.main.ctx.rotate(cell.rotation * Math.PI / 180);
                this.layers.main.ctx.drawImage(img, -this.options.cellSize / 2, -this.options.cellSize / 2, this.options.cellSize, this.options.cellSize);
                this.layers.main.ctx.restore();
            }
        }
    }
    boardtrainDraw(train, tracing) {
        if (tracing) {
            var train_size = this.options.cellSize;
            this.layers.trace.ctx.save();
            this.layers.trace.ctx.translate(train.c * this.options.cellSize + this.options.cellSize / 2, train.r * this.options.cellSize + this.options.cellSize / 2);
            this.layers.trace.ctx.fillStyle = train.color.name;
            this.layers.trace.ctx.fillRect(-train_size / 2, -train_size / 2, train_size, train_size);
            this.layers.trace.ctx.restore();
        } else {
            var train_size = this.options.cellSize / 2;
            var rot = 0;
            switch (train.cellPosition.toUpperCase()) {
                case "L": rot += 90;
                case "D": rot += 90;
                case "R": rot += 90;
            }
            var img = Palette.train_white.el;
            switch (train.color) {
                case Colors.Red: img = Palette.train_red.el; break;
                case Colors.Blue: img = Palette.train_blue.el; break;
                case Colors.Purple: img = Palette.train_purple.el; break;
            }
            this.layers.foreground.ctx.save();
            this.layers.foreground.ctx.translate(train.c * this.options.cellSize + this.options.cellSize / 2, train.r * this.options.cellSize + this.options.cellSize / 2);
            this.layers.foreground.ctx.rotate(rot * Math.PI / 180);

            if (train.crashed) {
                this.layers.foreground.ctx.fillStyle = "rgba(255,255,0,0.5)";
                this.layers.foreground.ctx.beginPath();
                this.layers.foreground.ctx.arc(0, 0, train_size * 4, 0, 2 * Math.PI);
                this.layers.foreground.ctx.fill();
                //~ this.layers.foreground.ctx.fillRect(-train_size*4/2,-train_size*4/2,train_size*4,train_size*4);
            }
            // Circle-style train
            /*
            ctx_fg.beginPath();
            ctx_fg.arc(0, 0, train_size*1.3/2, 0, 2 * Math.PI, false);
            ctx_fg.fillStyle = "grey";
            ctx_fg.fill();
            //~ ctx_fg.fillRect(-(train_size*1.2)/2,-(train_size*1.2)/2,train_size*1.2,train_size*1.2); // Border
            
            ctx_fg.beginPath();
            ctx_fg.arc(0, 0, train_size/2, 0, 2 * Math.PI, false);
            ctx_fg.fillStyle = Board.T_C[tr[4]];
            ctx_fg.fill();
            //~ ctx_fg.fillRect(-train_size/2,-train_size/2,train_size,train_size);
            */
            // Image-based train
            this.layers.foreground.ctx.drawImage(img, -this.options.cellSize / 2, -this.options.cellSize / 2, this.options.cellSize, this.options.cellSize);
            this.layers.foreground.ctx.restore();

            this.layers.trail.ctx.save();
            // Trail
            this.layers.trail.ctx.translate(train.c * this.options.cellSize + this.options.cellSize / 2, train.r * this.options.cellSize + this.options.cellSize / 2);
            this.layers.trail.ctx.fillStyle = train.color.name;
            this.layers.trail.ctx.fillRect(-this.options.trailSize / 2, -this.options.trailSize / 2, this.options.trailSize, this.options.trailSize);
            this.layers.trail.ctx.restore();
        }
    }
    boardOnBeforeTrainsDraw() {
        this.layers.foreground.ctx.clearRect(0, 0, this.layers.foreground.ctx.canvas.width, this.layers.foreground.ctx.canvas.height);

        this.layers.trail.ctx.save();
        this.layers.trail.ctx.fillStyle = "rgba(0,0,0," + this.options.trailFadeout + ")";
        this.layers.trail.ctx.globalCompositeOperation = "destination-out";
        this.layers.trail.ctx.fillRect(0, 0, this.layers.trail.ctx.canvas.width, this.layers.trail.ctx.canvas.height);
        this.layers.trail.ctx.restore();
    }

    static fromCharModel(playgroundContainer, charBoard){
        var blockBoard = charToBoard(charBoard);
        var board = new Board(blockBoard.board);
        return new Playground(playgroundContainer, board, blockBoard.highlightRects);
    }
}