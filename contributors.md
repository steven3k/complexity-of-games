---
title: Contributors
layout: default
---

## Admin Contacts:

- [Luciano Gualà](http://www.mat.uniroma2.it/~guala/). Email: {surname}@mat.uniroma2.it
- [Stefano Leucci](https://www.stefanoleucci.com). Email: {name}.{surname}@univaq.it

For technical matters please contact Stefano Leucci.

## Contributors:

- Matteo Almanza
- Emilio Cruciani
- Giorgio Ciotti
- Luca Di Donato
- Arno Gobbin
- Luciano Gualà
- Stefano Leucci
- Emanuele Natale
- André Nusser
- Roberto Tauraso
- Ben Wiederhake

Special thanks to [Franceca Marmigi](http://www.francescamarmigi.it/) for the CoG logo. 
